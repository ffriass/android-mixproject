package com.curso.datetime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String URL_KEY = "URL_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button diarioLBtn = findViewById(R.id.firstBtn);
        diarioLBtn.setTag("diai");
        diarioLBtn.setOnClickListener(this);
        Button seondBtn = findViewById(R.id.secondBtn);
        seondBtn.setTag("wired");
        seondBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, RSSActivity.class);
        intent.putExtra(URL_KEY, (String) v.getTag());
        startActivity(intent);
    }
}
