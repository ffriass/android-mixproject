package com.curso.datetime.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.curso.datetime.R;
import com.curso.datetime.entities.MyAudio;

import java.util.List;

/**
 * Created by Echenique on 9/4/18.
 */

public class MediaAdapter  extends RecyclerView.Adapter<MediaAdapter.MyRecycleItemViewHolder> {

    private List<MyAudio> items;
    private MediaListener listener;
    private Context context;
    private int positionSelected = -1;

    public MediaAdapter(Context context, List<MyAudio> items, MediaListener listener ) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    @Override
    public MyRecycleItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.media_layout, parent, false);
        MediaAdapter.MyRecycleItemViewHolder holder = new MediaAdapter.MyRecycleItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyRecycleItemViewHolder holder, int position) {
        MyAudio audio = items.get(position);
        audio.position = position;
        holder.status.setTag(audio);
        holder.status.setOnClickListener(clickListener);
        if( position == positionSelected) {
            holder.status.setText("playing");
        } else {
            holder.status.setText("stoped");
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MyAudio audio = (MyAudio) v.getTag();
            if( positionSelected == audio.position) {
                listener.stop();
                positionSelected = -1;
            } else {
                listener.play(audio.audioUrl);
                positionSelected = audio.position;
            }

        }
    };
    public static class MyRecycleItemViewHolder extends RecyclerView.ViewHolder{
        public TextView audio;
        public TextView status;

        public MyRecycleItemViewHolder(View itemView) {
            super(itemView);
            audio = itemView.findViewById(R.id.audio);
            status = itemView.findViewById(R.id.status);
        }
    }

    public interface MediaListener {
        void play(String audioUrl);
        void stop();
    }
}
