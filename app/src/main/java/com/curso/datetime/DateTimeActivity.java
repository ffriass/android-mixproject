package com.curso.datetime;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DateTimeActivity extends AbstractBaseActivity implements View.OnClickListener {

    private TextView dateDisplay, timeDisplay, dateTimeDisplay;

    private DatePickerDialog datePicker;
    private TimePickerDialog timePicker;
    private Calendar calendar;
    private SimpleDateFormat dateFormat, timeFormat, dateTimeFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_time);

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        timeFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
        dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());

        datePicker = new DatePickerDialog(this, datePickerListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        timePicker = new TimePickerDialog(DateTimeActivity.this, timePickerListener,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true);

        findViewById(R.id.date_pick).setOnClickListener(this);
        findViewById(R.id.time_pick).setOnClickListener(this);

        dateDisplay = (TextView) findViewById(R.id.date_display);
        timeDisplay = (TextView) findViewById(R.id.time_display);
        dateTimeDisplay = (TextView) findViewById(R.id.date_time_display);

        refreshDisplays();
    }

    @Override
    protected void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    protected RecyclerView.ViewHolder oncreateViewHolder(ViewGroup parent, int viewtype) {
        return null;
    }

    @Override
    protected int getItemViewType() {
        return 0;
    }

    @Override
    protected int getItemCounts() {
        return 0;
    }

    @Override
    public String getActitvityName() {
        return "Pantalla  del Horairi";
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.date_pick:
                datePicker.show();
                break;
            case R.id.time_pick:
                timePicker.show();
                break;
        }
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            refreshDisplays();
        }
    };

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DATE, dayOfMonth);

            refreshDisplays();
        }
    };

    private void refreshDisplays() {
        dateDisplay.setText(dateFormat.format(calendar.getTime()));
        timeDisplay.setText(timeFormat.format(calendar.getTime()));
        dateTimeDisplay.setText(dateTimeFormat.format(calendar.getTime()));

    }
}
