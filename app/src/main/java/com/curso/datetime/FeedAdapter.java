package com.curso.datetime;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.prof.rssparser.Article;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Echenique on 5/12/18.
 */

public class FeedAdapter extends ArrayAdapter<Article> {

    private List<Article> items;
    private Context context;

    public FeedAdapter(@NonNull Context context, ArrayList<Article> items) {
        super(context, 0, items);
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.rss_item, parent, false);
        }
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //((RssParserActivity) context).showArticle();
            }
        });
        ((TextView)rowView.findViewById(R.id.feedTitle)).setText(items.get(position).getTitle());
        return rowView;
    }
}
