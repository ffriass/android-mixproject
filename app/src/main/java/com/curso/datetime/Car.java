package com.curso.datetime;

/**
 * Created by Echenique on 5/19/18.
 */

public class Car  {

    public String brand;
    public String country;

    public Car(String brand, String country) {
        this.brand = brand;
        this.country = country;
    }
}
