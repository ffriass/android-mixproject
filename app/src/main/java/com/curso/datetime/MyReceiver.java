package com.curso.datetime;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {
    public static String MY_RECIEVER_ACTION = "com.vinrosa.broadcastreceivers.CUSTOM_INTENT";

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context,"Recibiendo", Toast.LENGTH_LONG).show();
    }
}
