package com.curso.datetime;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Echenique on 5/19/18.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyRecycleItemViewHolder> {

    private final List<Car> items;
    private final Context context;

    private int NORMAL_CARD = 0;
    private int MIDDLE_CARD = 1;
    private int BIG_CARD = 2;

    private static ItemListener listener;

    public MyRecyclerAdapter(Context context, List<Car> cars, ItemListener listener){
        this.context = context;
        this.items = cars;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if( position == 0){
            return BIG_CARD;
        } else if ( position % 2 == 0){
            return MIDDLE_CARD;
        } else {
            return NORMAL_CARD;
        }
    }

    @Override
    public MyRecycleItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = R.layout.card_view;
        if(viewType == BIG_CARD){
            layout = R.layout.card_view_big;
        } else if ( viewType == MIDDLE_CARD){
            layout = R.layout.card_view_middle;
        }
        View view = LayoutInflater.from(context).inflate(layout, parent, false);
        MyRecycleItemViewHolder holder = new MyRecycleItemViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyRecycleItemViewHolder holder, int position) {
        Car car = items.get(position);
        holder.country.setText(car.country);
        holder.brand.setText(car.brand);


        holder.itemView.setTag(car);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Car car = (Car) v.getTag();
                listener.carClicked(car);

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class MyRecycleItemViewHolder extends RecyclerView.ViewHolder{
        public TextView brand;
        public TextView country;

        public MyRecycleItemViewHolder(View itemView) {
            super(itemView);
            brand = itemView.findViewById(R.id.brand);
            country = itemView.findViewById(R.id.country);
        }
    }

    public interface ItemListener{
        void carClicked(Car car);
    }
}
