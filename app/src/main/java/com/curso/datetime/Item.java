package com.curso.datetime;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by Echenique on 6/9/18.
 */
@Entity
public class Item {

    @Id
    public Integer id;

    @Expose
    public String label;

    @Expose
    public String description;

    @Expose
    @SerializedName("image_url")
    public String imageUrl;

    @Expose
    public int priority;

    @Generated(hash = 809845590)
    public Item(Integer id, String label, String description, String imageUrl,
            int priority) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.imageUrl = imageUrl;
        this.priority = priority;
    }
    @Generated(hash = 1470900980)
    public Item() {
    }

    public Item(Object o, String item, String description, int i) {

    }

    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return this.label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getPriority() {
        return this.priority;
    }
    public void setPriority(int priority) {
        this.priority = priority;
    }
    public String getImageUrl() {
        return this.imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
