package com.curso.datetime;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewActivity extends AppCompatActivity implements MyRecyclerAdapter.ItemListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(new MyRecyclerAdapter(this,getCars(),this));
    }


    private List<Car> getCars(){
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Honda","Japon"));
        cars.add(new Car("Honda","Japon"));
        cars.add(new Car("Honda","Japon"));
        cars.add(new Car("Honda","Japon"));
        cars.add(new Car("Honda","Japon"));
        cars.add(new Car("Toyota","Japon"));
        return cars;
    }

    @Override
    public void carClicked(Car car) {


    }


}
