package com.curso.datetime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.curso.datetime.activities.GDNewActivity;

import java.util.List;

import static com.curso.datetime.activities.GDNewActivity.ACITVITY_CEATED;

/**
 * Created by Echenique on 6/9/18.
 */

public class GDActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int CREATE_ACTIVITY = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_item);
        getItems();
    }

    private void getItems() {
        DaoSession daoSession = ((MyApp) getApplication()).getDaoSession();
        ItemDao itemDao = daoSession.getItemDao();
        Item item = new Item(null,"Item","Description",0);
        itemDao.insert(item);
        List<Item> items = itemDao.loadAll();
        //TODO set items in recycler view
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CREATE_ACTIVITY){
            if( data.getExtras().getBoolean(ACITVITY_CEATED)){
                getItems();
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getItems();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, GDNewActivity.class);
        startActivityForResult(intent, CREATE_ACTIVITY);
    }
}
