package com.curso.datetime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by Echenique on 10/13/18.
 */

public abstract class AbstractBaseActivity extends AppCompatActivity {

    private ViewGroup parent;
    private Car[] cars;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setActivityName(getActitvityName());
        //setActivityName(getActitvityName());
        parent=  null; // findop
        for(int i = 0; i < getItemCounts(); i++ ){
            int  viewtype = getItemViewType();
            RecyclerView.ViewHolder holder = oncreateViewHolder(parent, viewtype);
            onBindViewHolder(holder,i);
        }


    }

    public Car getCar(){
        return null;
    }

    protected abstract void onBindViewHolder(RecyclerView.ViewHolder holder, int position);

    protected abstract  RecyclerView.ViewHolder oncreateViewHolder(ViewGroup parent, int viewtype);

    protected abstract int getItemViewType();

    protected abstract int getItemCounts();

    private void setActivityName(String name){
        //TODO send activity name to GA
    }

    public abstract String getActitvityName();

}
