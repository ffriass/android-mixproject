package com.curso.datetime.activities;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.curso.datetime.Item;
import com.curso.datetime.MyReceiver;
import com.curso.datetime.R;
import com.curso.datetime.api.ApiClient;
import com.curso.datetime.api.MyRemoteService;
import com.curso.datetime.utils.SharedData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitActivity extends AppCompatActivity {

    private static final String KEY = "KEY";

    private MyReceiver receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retrofit);
        MyRemoteService myRemoteService = ApiClient.getApiClient().create(MyRemoteService.class);
        Call<List<Item>>  itemsCall = myRemoteService.getItems();
        itemsCall.enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                List<Item> items = response.body();
                //TODO add items to recycler list
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {

            }
        });

        boolean valueSetted = SharedData.setValue(this, KEY, "un valor");
        if( valueSetted) {
            String valueSet = SharedData.getValue(this, KEY);
            System.out.print(valueSet);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter(MyReceiver.MY_RECIEVER_ACTION);
        registerReceiver(receiver, filter);

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }
}
