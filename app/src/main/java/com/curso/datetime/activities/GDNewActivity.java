package com.curso.datetime.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import static com.curso.datetime.GDActivity.CREATE_ACTIVITY;

public class GDNewActivity extends AppCompatActivity {

    public static final String ACITVITY_CEATED = "ACITVITY_CEATED";

    public void createActivity(){
        Intent intentResult = new Intent();
        intentResult.putExtra(ACITVITY_CEATED, true);
        setResult(CREATE_ACTIVITY, intentResult);
        finish();
    }
}
