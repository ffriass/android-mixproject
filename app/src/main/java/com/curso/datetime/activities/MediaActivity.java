package com.curso.datetime.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.curso.datetime.R;
import com.curso.datetime.adapters.MediaAdapter;
import com.curso.datetime.services.MediaService;

import java.util.ArrayList;
import java.util.List;

import com.curso.datetime.entities.MyAudio;

/**
 * Created by Echenique on 9/4/18.
 */

public class MediaActivity extends AppCompatActivity implements MediaAdapter.MediaListener {

    private boolean serviceBound = false;

    private MediaService mediaService;

    private MediaAdapter mediaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(mediaAdapter = new MediaAdapter(this,getAudios(),this));

    }


    private List<MyAudio> getAudios() {
        List<MyAudio> audios = new ArrayList<>();
        audios.add(new MyAudio("https://firebasestorage.googleapis.com/v0/b/party-finder-android.appspot.com/o/audio%2Fl0DXiGpSLVZxAyypaZHnM2OCdBj1_20180831_214012.3gp?alt=media&token=1dce8d5e-75d3-49f7-80a9-080c61af145b"));
        audios.add(new MyAudio("https://firebasestorage.googleapis.com/v0/b/party-finder-android.appspot.com/o/audio%2Fl0DXiGpSLVZxAyypaZHnM2OCdBj1_20180831_214539.3gp?alt=media&token=62850735-b518-4e05-8aae-b8576ba7ae7b"));
        audios.add(new MyAudio("https://firebasestorage.googleapis.com/v0/b/party-finder-android.appspot.com/o/audio%2Fl0DXiGpSLVZxAyypaZHnM2OCdBj1_20180901_123113.3gp?alt=media&token=ced4d696-f02a-491e-bf7b-eab4b770baf6"));
        return audios;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to the service
        Intent intent = new Intent(this, MediaService.class);
        startService(intent);
        bindService(intent, mConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Unbind from the service
        if (serviceBound) {
            unbindService(mConnection);
            serviceBound = false;
        }
    }


    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mediaService = ((MediaService.MediaPlayerBinder) service).getService();
            serviceBound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            mediaService = null;
            serviceBound = false;
        }
    };

    @Override
    public void play(String audioUrl) {
        if(mediaService != null) {
            mediaService.play(audioUrl);
            mediaAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void stop() {
        if(mediaService != null) {
            mediaService.stop();
            mediaAdapter.notifyDataSetChanged();
        }
    }
}
