package com.curso.datetime.myapplication;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by Echenique on 9/1/18.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
