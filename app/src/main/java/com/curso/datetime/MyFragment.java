package com.curso.datetime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Echenique on 5/12/18.
 */

public class MyFragment extends Fragment implements View.OnClickListener {

    private TextView titleTv;
    private Button plusBtn;
    private Button minusBtn;

    private static String TITLE = "TITLE";

    public static MyFragment newInstance( String title){
        MyFragment frag = new MyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE,title);
        frag.setArguments(bundle);
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment,container);
        titleTv = rootView.findViewById(R.id.articleTitle);
        titleTv.setTextSize(initSize);
        plusBtn = rootView.findViewById(R.id.plus);
        plusBtn.setOnClickListener(this);
        minusBtn = rootView.findViewById(R.id.minus);
        minusBtn.setOnClickListener(this);

        String title = getArguments().getString(TITLE);
        return rootView;
    }

    public void setTitle(String title){
        titleTv.setText(title);
    }

    float maxSize = 30;
    float minSize = 12;
    float initSize = 20;


    @Override
    public void onClick(View v) {
        float size = titleTv.getTextSize();
        if( v == plusBtn ){
            if(size > maxSize){
                size = maxSize;
            }
           titleTv.setTextSize(++size);
        } else if ( v == minusBtn ){
            if(size < minSize){
                size = minSize;
            }
            titleTv.setTextSize(--size);
        }
    }
}
