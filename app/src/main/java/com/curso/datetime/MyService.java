package com.curso.datetime;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Echenique on 5/26/18.
 */

public class MyService extends Service {

    public static String MY_INFO = "MY_INFO";
    public static String START_TIMER = "START_TIMER";
    public static String STOP_TIMER = "STOP_TIMER";
    public static String MY_SERVICE = "com.curso.datetime.MyService.MY_SERVICE";

    private int counter;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getExtras().getString(MY_INFO);
        if( action.equals(START_TIMER)){
            startTimer();
        } else if( action.equals(STOP_TIMER)){
            stopTimer();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void stopTimer() {
        if(timer != null){
            timer.cancel();
            timer = null;
        }
        Intent intent = new Intent(MY_SERVICE);
        intent.putExtra("","");
        LocalBroadcastManager.getInstance(MyService.this).sendBroadcast(intent);
    }

    private void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.i("in timer","in timer +++" + (counter++));
            }
        };
        timer.schedule(timerTask,1000,1000);
    }

    private Timer timer;
    private TimerTask timerTask;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
