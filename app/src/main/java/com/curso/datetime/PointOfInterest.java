package com.curso.datetime;

import android.location.Location;
import android.location.LocationManager;
import android.text.TextUtils;

/**
 * Created by Echenique on 7/17/18.
 */

public class PointOfInterest {

    public static final int TEE = 1;
    public static final int GREEN = 2;

    public String name;
    public String latitude;
    public String longitude;
    public int type;


    public Location getGeoLocation() {
        Location poiLocation = new Location(LocationManager.PASSIVE_PROVIDER);
        poiLocation.setLatitude(getLat());
        poiLocation.setLongitude(getLon());
        return poiLocation;
    }

    public double getLat() {
        double res = 0;
        if (!TextUtils.isEmpty(latitude)) {
            try {
                res = Double.parseDouble(latitude);
            }catch (NumberFormatException e){
                res = 0;
            }
        }
        return res;
    }

    public double getLon() {
        double res = 0;
        if (!TextUtils.isEmpty(longitude)) {
            try {
                res = Double.parseDouble(longitude);
            }catch (NumberFormatException e){
                res = 0;
            }
        }
        return res;
    }
}
