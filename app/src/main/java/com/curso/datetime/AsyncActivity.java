package com.curso.datetime;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Echenique on 5/25/18.
 */

public class AsyncActivity extends AppCompatActivity implements View.OnClickListener{

    EditText number;
    TextView progress;
    TextView result;
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);
        number = findViewById(R.id.my_number);
        progress = findViewById(R.id.progress);
        result = findViewById(R.id.isprime);
        btn = findViewById(R.id.startBtn);
        btn.setOnClickListener(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,new IntentFilter(MyService.MY_SERVICE));
    }

    String proccessString = "Procesando";



    private void updateWithHandler(){
        Looper looper = Looper.getMainLooper();
        final Handler handler = new Handler(looper);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                proccessString += ".";
                progress.setText(proccessString);
                handler.postDelayed(this,1000);
                if( proccessString.length() > 100){
                    handler.removeCallbacks(this);
                }
            }
        };

        handler.postDelayed(runnable,1000);
        looper.loop();
    }

    boolean isRunning;

    @Override
    public void onClick(View v) {
        //new FindPrimeNumbers().execute(Integer.valueOf(number.getText().toString()));
       // updateWithHandler();
        if(!isRunning) {
            isRunning = true;
            startMyService();
        } else {
            isRunning = false;
            stopMyService();
        }
    }

    private void stopMyService() {
        Intent intent = new Intent(this,MyService.class);
        intent.setAction(MyService.STOP_TIMER);
        intent.putExtra(MyService.MY_INFO,MyService.STOP_TIMER);
        startService(intent);
    }

    private void startMyService() {
        Intent intent = new Intent(this,MyService.class);
        intent.setAction(MyService.START_TIMER);
        intent.putExtra(MyService.MY_INFO,MyService.START_TIMER);
        startService(intent);

    }

    public synchronized boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n%2==0) return false;
        //if not, then just check the odds
        for(int i=3;i*i<=n;i+=2) {
            if(n%i==0)
                return false;
        }
        return true;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showNotification();
        }
    };

    private void showNotification(){
        Intent intent = new Intent(this, AsyncActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        int iconResId = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) ?
                R.drawable.ic_launcher_background : R.mipmap.ic_launcher;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(iconResId)
                .setColor(getResources().getColor(R.color.cardview_dark_background))
                .setContentTitle("titulo")
                .setContentText("mensaje")
                //.setContent()
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }


    public class FindPrimeNumbers extends AsyncTask<Integer, Integer, Integer>{

        @Override
        protected Integer doInBackground(Integer... numbers) {
            for(Integer number : numbers){
                for(int i = 0; i <= number; i++){
                    if( i != number) {
                        publishProgress(i);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        return number;
                    }
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if( integer != null){
                String isPrime = isPrime(integer) ? " es primo" : " no es primo";
                result.setText("El # " + integer + isPrime);
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            String isPrime = isPrime(values[0]) ? " es primo" : " no es primo";
            progress.setText("Procesando  " + values[0] + isPrime);
        }
    }
}
