package com.curso.datetime;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import augmentedreality.data.ARData;
import augmentedreality.ui.IconMarker;
import augmentedreality.ui.Marker;
import augmentedreality.view.AugmentedReality;

import static com.curso.datetime.PointOfInterest.TEE;

/**
 * Created by Echenique on 7/17/18.
 */

public class ARActivity extends AppCompatActivity implements LocationUtil.LocationAble {


    private AugmentedReality augmentedRealityView;
    private List<PointOfInterest>	listPoi;
    private Location                myLocation;
    private LocationUtil 		    locationUtil;
    public static final String      HOLES_JSON = "HOLES_JSON";

    private BuildUI buildUIAsyncTask;

    public static final int     MY_CAMERA_PERMISSON = 1233;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ar_layout);

//        locationUtil = new LocationUtil(this,this);
//        locationUtil.refreshLocation(this);
        String jsonString = "[{\"description\":\"Hoyo 1\",\"green\":{\"latitude\":18.4505342,\"longitude\":-69.956984},\"place_id\":19,\"tee\":{\"latitude\":18.565025,\"longitude\":-68.339449}}]";
        listPoi = initPois(jsonString);
        init();
    }

    private List<PointOfInterest> initPois(String jsonString) {
        List<PointOfInterest> places = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(jsonString);
            for(int i = 0; i < array.length(); i++){
                PointOfInterest place = new PointOfInterest();
                JSONObject obj = array.getJSONObject(i);
                place.name = obj.getString("description");
                place.latitude = obj.getJSONObject("green").getString("latitude");
                place.longitude = obj.getJSONObject("green").getString("longitude");
                place.type = PointOfInterest.GREEN;
                places.add(place);


//                PointOfInterest teePlace = new PointOfInterest();
//                teePlace.name = obj.getString("description");
//                teePlace.latitude = obj.getJSONObject("tee").getString("latitude");
//                teePlace.longitude = obj.getJSONObject("tee").getString("longitude");
//                place.type = TEE;
//
//
//                places.add(teePlace);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return places;
    }


    private void init() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSON);
            return;
        }
        LinearLayout augmentedRealityLayout = findViewById(R.id.augmented_reality_layout);
        if (augmentedRealityView == null) {
            augmentedRealityView = new AugmentedReality(this);
        }

        if (augmentedRealityLayout.getChildCount() == 0) {
            augmentedRealityLayout.addView(augmentedRealityView);
        }

        buildUIAsyncTask =new BuildUI();
        buildUIAsyncTask.execute();

    }


    class BuildUI extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... arg0) {
            addARMarkers();
            return null;
        }
    }

    private void addARMarkers() {
        ARData.clear();
        ARData.setCurrentLocation(myLocation);
        List<Marker> markers = new ArrayList<Marker>();
        if (listPoi != null && listPoi.size() > 0) {
            for (PointOfInterest hole : listPoi) {
                Marker marker = new IconMarker(hole, hole.name, hole.getLat(), hole.getLon(), 0, Color.RED, getMarkerIcon(hole.type != TEE ? R.drawable.green_pin_icn : R.drawable.tee_pin_icn));
                markers.add(marker);
            }
        }
        ARData.addMarkers(markers);
        Log.d("Markets", "markets -> " + markers.size());
        Log.d("AR Markers ->", " " + ARData.getMarkers().size());
    }


    @Override
    public void onResume() {
        super.onResume();
        if (augmentedRealityView != null && !augmentedRealityView.isARPresent()) {
            augmentedRealityView.resumeAR();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (buildUIAsyncTask != null && buildUIAsyncTask.getStatus() == AsyncTask.Status.RUNNING){
            buildUIAsyncTask.cancel(true);
        }
    }

    private void releaseAR() {
        if (augmentedRealityView != null) {
            augmentedRealityView.releaseAR();
            augmentedRealityView = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseAR();
    }


    private Bitmap getMarkerIcon(int icon) {
        return BitmapFactory.decodeResource(getResources(), icon);
    }


    @Override
    public void updateLocation(Location location) {
        myLocation = location;
        new BuildUI().execute();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_CAMERA_PERMISSON: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    init();

                }
                return;
            }

        }
    }
}
