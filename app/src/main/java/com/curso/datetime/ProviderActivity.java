package com.curso.datetime;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Echenique on 6/9/18.
 */

public class ProviderActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rss_item);
        ContentValues values = new ContentValues();
        values.put("description","Description 1");
        new SimpleContentProvider().insert(SimpleContentProvider.CONTENT_URI,values);
        getSupportLoaderManager().initLoader(1,null,this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, SimpleContentProvider.CONTENT_URI,null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if(cursor == null) return;
        while(cursor.moveToNext()) {
            String word = cursor.getString(2);
            ((TextView) findViewById(R.id.feedTitle)).setText(word);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
