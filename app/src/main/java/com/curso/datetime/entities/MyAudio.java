package com.curso.datetime.entities;

/**
 * Created by Echenique on 9/4/18.
 */

public class MyAudio {
    public String audioUrl;
    public String name;
    public int position;

    public MyAudio(String s) {
        this.audioUrl = s;
    }
}
