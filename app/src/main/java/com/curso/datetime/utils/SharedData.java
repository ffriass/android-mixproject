package com.curso.datetime.utils;

import android.content.Context;

/**
 * Created by Echenique on 9/1/18.
 */

public class SharedData {

    public static final String PREF_NAME = "com.curso.datetime.PREF_NAME";

    public static boolean setValue(Context ctx, String key, String value) {
            boolean updated = ctx.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
                    .edit().putString(key,value).commit();
            return updated;
    }

    public static String getValue(Context ctx, String key) {
        return ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getString(key,"");
    }
}
