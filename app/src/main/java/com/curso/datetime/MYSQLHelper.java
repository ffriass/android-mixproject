package com.curso.datetime;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Echenique on 6/2/18.
 */

public class MYSQLHelper extends SQLiteOpenHelper {

    private static int MY_DB_VERSION = 1;
    private static String DB_NAME = "m_db";
    public static String FIELD_WORD = "word";
    public static String FIELD_DEFINITION = "defination";
    private static String TABLE_NAME = "dictionary";
    private static String TABLE_NAME_CREATE = "CREATE TABLE " + TABLE_NAME + " ( " + FIELD_WORD + " TEXT, "+ FIELD_DEFINITION + " TEXT ) ";

    public MYSQLHelper(Context context) {
        super(context, DB_NAME, null, MY_DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_NAME_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long createEntity(String word, String defination){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FIELD_WORD,word);
        values.put(FIELD_DEFINITION,defination);
        return db.insert(TABLE_NAME,null,values);
    }

    public Cursor selectEntities(){
        SQLiteDatabase db = getReadableDatabase();
        String[] cols = new String[]{ FIELD_WORD, FIELD_DEFINITION };
        return db.query(TABLE_NAME,cols,null,null,null,null,null);
    }
}
