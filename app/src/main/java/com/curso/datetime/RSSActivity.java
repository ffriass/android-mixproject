package com.curso.datetime;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.prof.rssparser.Article;
import com.prof.rssparser.Parser;

import java.util.ArrayList;

import static com.curso.datetime.MainActivity.URL_KEY;

public class RSSActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss);
        final ListView rssLisr = findViewById(R.id.rssList);
        rssLisr.setOnItemClickListener(this);
        final Spinner loading = findViewById(R.id.loading);
        String url = getIntent().getExtras().getString(URL_KEY);
        Parser parser = new Parser();
        parser.onFinish(new Parser.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(ArrayList<Article> list) {
                loading.setVisibility(View.GONE);
                rssLisr.setAdapter(new FeedAdapter(RSSActivity.this, list));
            }

            @Override
            public void onError() {

            }
        });
        parser.execute(url);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Article article = (Article) parent.getSelectedItem();
        if(position%2 == 0){
            //Par
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(article.getLink()));
            startActivity(intent);
        } else {
            //None
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(URL_KEY, article.getLink());
            startActivity(intent);
        }
    }
}
