package com.curso.datetime;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.greendao.database.Database;

/**
 * Created by Echenique on 6/9/18.
 */

public class MyApp extends Application {

    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,"notes-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Stetho.initializeWithDefaults(this);
    }

    public DaoSession getDaoSession(){
        return daoSession;
    }
}
