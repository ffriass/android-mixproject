package com.curso.datetime.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Echenique on 9/1/18.
 */

public class FormResponse {

    @Expose
    public String message;

    @SerializedName("form")
    @Expose
    public FormObject formBody;

    public String otro;
    public String otrov;
    public String other;
}
