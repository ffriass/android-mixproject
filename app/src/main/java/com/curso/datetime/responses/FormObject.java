package com.curso.datetime.responses;

import com.google.gson.annotations.Expose;

/**
 * Created by Echenique on 9/1/18.
 */

class FormObject {
    @Expose
    public String name;
}
