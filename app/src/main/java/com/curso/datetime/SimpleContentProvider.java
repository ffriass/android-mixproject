package com.curso.datetime;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Echenique on 6/9/18.
 */

public class SimpleContentProvider extends ContentProvider {

    private SQLiteDatabase database;
    private DaoSession daoSession;
    public static String URI_STRING = "content://com.curso.datetime.simplecontentprovider/notes";
    public static Uri CONTENT_URI = Uri.parse(URI_STRING);

    @Override
    public boolean onCreate() {
        daoSession = ((MyApp) getContext().getApplicationContext()).getDaoSession();
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(ItemDao.TABLENAME);
        Cursor cursor = builder.query(database,projection,selection,selectionArgs,null,null,sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        ItemDao dao = daoSession.getItemDao();
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
