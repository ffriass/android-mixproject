package com.curso.datetime;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by Echenique on 6/2/18.
 */

public class MySQLActivity extends AppCompatActivity {

    private  MYSQLHelper helper;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_sql_activity);
        helper = new MYSQLHelper(this);
        tv = findViewById(R.id.text);
        helper.createEntity("palabra","definicion");
        Cursor cursor = helper.selectEntities();
        while(cursor.moveToNext()) {
            int index;

            index = cursor.getColumnIndexOrThrow(MYSQLHelper.FIELD_WORD);
            String word = cursor.getString(index);

            index = cursor.getColumnIndexOrThrow(MYSQLHelper.FIELD_DEFINITION);
            String definition = cursor.getString(index);
            tv.setText(word + definition);

        }

    }
}
