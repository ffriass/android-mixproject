package com.curso.datetime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.prof.rssparser.Article;
import com.prof.rssparser.Parser;

import java.util.ArrayList;
import java.util.List;

public class RssParserActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    MyFragment fragment;
    List<Article> articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_parser);
        final ListView feedListView = findViewById(R.id.feedListView);
        feedListView.setOnItemClickListener(this);
        String url = "https://www.wired.com/feed/rss";
        Parser parser = new Parser();
        parser.onFinish(new Parser.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(ArrayList<Article> list) {
                articles = list;
                feedListView.setAdapter(new FeedAdapter(RssParserActivity.this,list));
            }

            @Override
            public void onError() {

            }
        });
        parser.execute(url);

        fragment = (MyFragment) getSupportFragmentManager().findFragmentById(R.id.my_fragment);

       // fragment = MyFragment.newInstance("title");

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RssParserActivity.this,WebViewActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        fragment.setTitle(articles.get(position).getTitle());
    }

    public void showArticle(Article article){
        //TODO show articel
    }
}
