package com.curso.datetime.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Echenique on 9/3/18.
 */

public class MediaService extends Service {

    private final MediaPlayerBinder playerBinder = new MediaPlayerBinder();

    private MediaPlayer mMediaPlayer = new MediaPlayer();

    public void play(String audioUrl) {
        stop();
        try {
            mMediaPlayer.setDataSource(audioUrl);
            mMediaPlayer.prepare();
        } catch (Exception e) {
            Log.e("MediaService", e.getMessage());
        }
        mMediaPlayer.start();
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stop();
            }
        });
    }

    public void stop() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
            mMediaPlayer.reset();
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return playerBinder;
    }


    public class MediaPlayerBinder extends Binder {
        public MediaService getService() {
            return MediaService.this;
        }
    }
}
