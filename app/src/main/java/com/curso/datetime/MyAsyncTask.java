package com.curso.datetime;

import android.os.AsyncTask;

/**
 * Created by Echenique on 5/26/18.
 */

public class MyAsyncTask extends AsyncTask<Void,Void,Void> {

    private MyListener listener;

    public MyAsyncTask(MyListener listener){
        this.listener = listener;
    }

    public interface MyListener {
        void onProgreesUpdate(Integer number);
        void onPostexcute();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        listener.onProgreesUpdate(1);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        listener.onPostexcute();
    }
}
