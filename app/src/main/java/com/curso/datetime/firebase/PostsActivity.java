package com.curso.datetime.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.curso.datetime.R;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class PostsActivity extends AuthAbstratActivity implements View.OnClickListener, Loadmodelable {

    private Button loginBtn;
    private View btnContainer;
    private View registerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        loginBtn = findViewById(R.id.loginBtn);
        registerBtn = findViewById(R.id.registerBtn);
        btnContainer = findViewById(R.id.btnContainer);
        // Write a message to the database
        FirebaseHelper.getTable("posts", Post.class, this);
        FirebaseHelper.getTable("users", User.class, this);

    }

    @Override
    public String getActivityName() {
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if( currentUser == null) {
            btnContainer.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        if ( v ==  loginBtn) {
            startActivity(new Intent(this, LoginActivity.class));
        } else  if ( v == registerBtn ) {
            startActivity(new Intent(this, RegisterActivity.class));
        }

    }

    @Override
    public void onStartLoading(String node) {

    }

    @Override
    public void onFinished(List<BaseEntity> posts, String node) {

    }
}
