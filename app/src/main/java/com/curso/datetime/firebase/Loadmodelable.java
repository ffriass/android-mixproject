package com.curso.datetime.firebase;

import java.util.List;

public interface Loadmodelable {
    void onStartLoading(String node );
    void onFinished(List<BaseEntity> posts, String node);
}
