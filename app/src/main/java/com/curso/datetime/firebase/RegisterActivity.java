package com.curso.datetime.firebase;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.curso.datetime.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class RegisterActivity extends AuthAbstratActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiter);
        user = findViewById(R.id.user);
        pass = findViewById(R.id.pass);
        mAuth.createUserWithEmailAndPassword(user.getText().toString(),
                pass.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                } else {

                }
            }
        });
    }

    @Override
    public String getActivityName() {
        return null;
    }
}
