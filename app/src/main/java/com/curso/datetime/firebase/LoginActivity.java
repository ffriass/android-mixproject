package com.curso.datetime.firebase;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Toast;

import com.curso.datetime.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class LoginActivity extends AuthAbstratActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        user = findViewById(R.id.user);
        pass = findViewById(R.id.pass);
        findViewById(R.id.loginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO validate if user and pass are not empty
                mAuth.signInWithEmailAndPassword(user.getText().toString(),
                        pass.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    //User is logged in
                                } else {
                                    Toast.makeText(LoginActivity.this, "Pass o Usuario incorrecto", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });

    }

    public void register(String user, String pass, String name , String last_name){
        mAuth.createUserWithEmailAndPassword(user, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    task.getResult().getUser().getUid();
                    //TODO intert DB /users/uid .push(user)
                } else {
                    //TODO manage error
                }
            }
        });
    }

    @Override
    public String getActivityName() {
        return "LoginAcivity";
    }
}
