package com.curso.datetime.firebase;

import java.util.List;

public class Post extends BaseEntity {
    public String name;
    public String address;
    public List<String> comments;
    public String imageUrl;
}
