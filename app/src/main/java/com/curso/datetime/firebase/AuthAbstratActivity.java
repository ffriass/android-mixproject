package com.curso.datetime.firebase;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

public abstract class AuthAbstratActivity extends AppCompatActivity {

    protected FirebaseAuth mAuth;
    protected EditText user;
    protected EditText pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        sendAnalytics(getActivityName());
    }

    private void sendAnalytics(String activityName) {
        //TODO send name to GA
    }

    public abstract String getActivityName();
}
