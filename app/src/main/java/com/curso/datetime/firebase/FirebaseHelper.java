package com.curso.datetime.firebase;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Echenique on 11/16/17.
 */

public class FirebaseHelper {

    private static boolean authSuccess;

    public static boolean isAuthSuccess(){
        return authSuccess;
    }

    public static void getTable(final String node, final Class entity, final Loadmodelable listener){
        DatabaseReference db  = FirebaseDatabase.getInstance().getReference().child(node);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onStartLoading(node);
                List<BaseEntity> result = new ArrayList<>();
                for (DataSnapshot noteSnapshot: dataSnapshot.getChildren()){
                    BaseEntity model = (BaseEntity) noteSnapshot.getValue(entity);
                    // model.saveModel();
                    result.add(model);
                }
                listener.onFinished(result,node);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FirebaseHelper", databaseError.getMessage());
            }
        });

    }

    public static void getObject(final String node, final Class entity, final Loadmodelable listener){
        DatabaseReference db  = FirebaseDatabase.getInstance().getReference().child(node);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onStartLoading(node);
                List<BaseEntity> result = new ArrayList<>();
                BaseEntity model = (BaseEntity) dataSnapshot.getValue(entity);
                result.add(model);
                listener.onFinished(result,node);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FirebaseHelper", databaseError.getMessage());
            }
        });

    }

    public static void monitorObject(final String node, final Class entity, final Loadmodelable listener){
        DatabaseReference db  = FirebaseDatabase.getInstance().getReference().child(node);
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onStartLoading(node);
                List<BaseEntity> result = new ArrayList<>();
                BaseEntity model = (BaseEntity) dataSnapshot.getValue(entity);
                result.add(model);
                listener.onFinished(result,node);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FirebaseHelper", databaseError.getMessage());
            }
        });

    }

    public static void getObjectChild(final String node, final String child, final Class entity, final Loadmodelable listener){
        DatabaseReference db  = FirebaseDatabase.getInstance().getReference().child(node).child(child);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listener.onStartLoading(node);
                List<BaseEntity> result = new ArrayList<>();
                BaseEntity model = (BaseEntity) dataSnapshot.getValue(entity);
                if(model != null) {
                    result.add(model);
                }
                listener.onFinished(result,node);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("FirebaseHelper", databaseError.getMessage());
            }
        });

    }



//    public static void fireBaseAuthenticate(final Authable listener) {
//        FirebaseAuth mAuth = FirebaseAuth.getInstance();
//        mAuth.signInAnonymously()
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        listener.authenticationResult(authSuccess = task.isSuccessful(),"");
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                listener.authenticationResult(authSuccess = false,"");
//            }
//        });

//    }
}

