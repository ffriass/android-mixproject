package com.curso.datetime.api;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Echenique on 9/1/18.
 */

public class ApiClient {

    public static Retrofit instance;

    public static Retrofit getApiClient() {
        if( instance == null) {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
            instance = new Retrofit.Builder()
                    .baseUrl("https://vinrosa.com/")
                    .addConverterFactory(GsonConverterFactory.create(
                            new GsonBuilder()
                                    .excludeFieldsWithoutExposeAnnotation()
                                    .create()))
                    .client(client)
                    .build();
        }
        return instance;
    }
}
