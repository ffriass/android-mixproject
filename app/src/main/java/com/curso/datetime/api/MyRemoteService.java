package com.curso.datetime.api;

import com.curso.datetime.Item;
import com.curso.datetime.responses.FormResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Echenique on 9/1/18.
 */

public interface MyRemoteService {

    @GET("example/practica.json")
    Call<List<Item>> getItems();

    @FormUrlEncoded
    @POST("example/form.php")
    Call<FormResponse> postForm(@Field("name") String  nameValue);
}
