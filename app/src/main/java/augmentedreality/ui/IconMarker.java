package augmentedreality.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import augmentedreality.ui.object.PaintableIcon;


/**
 * This class extends Marker and draws an icon instead of a circle for it's visual representation.
 * 
 * @author nbautista
 */
public class IconMarker extends Marker {
    private Bitmap bitmap = null;

    public IconMarker(String name, double latitude, double longitude, double altitude, int color, Bitmap bitmap) {
        super(name, latitude, longitude, altitude, color);
        this.bitmap = bitmap;
    }

    public IconMarker(Object obj, String name, double latitude, double longitude, double altitude, int color, Bitmap bitmap) {
        super(obj, name, latitude, longitude, altitude, color);
        this.bitmap = bitmap;
    }
	/**
	 * {@inheritDoc}
	 */
    @Override
    public void drawIcon(Canvas canvas) {
    	if (canvas==null || bitmap==null) throw new NullPointerException();

        if (gpsSymbol==null) gpsSymbol = new PaintableIcon(bitmap);//, 45, 45);
        super.drawIcon(canvas);
    }
}
