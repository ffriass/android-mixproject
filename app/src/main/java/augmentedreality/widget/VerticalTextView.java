package augmentedreality.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextPaint;
import android.util.AttributeSet;

import augmentedreality.view.AugmentedReality;


/**
 * This class extends the TextView class and is designed to work vertically and horizontally.
 * 
 * @author @author nbautista
 */
public class VerticalTextView extends AppCompatTextView {

	public VerticalTextView(Context context) {
		super(context);
	}
	
	public VerticalTextView(Context context, AttributeSet attrs){
		super(context, attrs);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		if (AugmentedReality.portrait) {
			super.onMeasure(heightMeasureSpec, widthMeasureSpec);
			setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onDraw(Canvas canvas){
		if (AugmentedReality.portrait) {
			TextPaint textPaint = getPaint();
			textPaint.setColor(getCurrentTextColor());
			textPaint.drawableState = getDrawableState();

			canvas.save();
			canvas.translate(0, getHeight());
			canvas.rotate(-90);
			canvas.translate(getCompoundPaddingLeft(), getExtendedPaddingTop());
			getLayout().draw(canvas);
			canvas.restore();
		} else {
			super.onDraw(canvas);
		}
	}
}
