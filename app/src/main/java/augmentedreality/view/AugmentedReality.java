package augmentedreality.view;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.curso.datetime.R;

import java.text.DecimalFormat;

import augmentedreality.camera.CameraSurface;
import augmentedreality.data.ARData;
import augmentedreality.ui.Marker;
import augmentedreality.widget.VerticalTextView;


/**
 * This class extends the SensorsActivity and is designed tie the AugmentedView and zoom bar together.
 * 
 * @author nbautista
 */
public class AugmentedReality extends SensorsActivity implements OnTouchListener {
	private static final String TAG						= "AugmentedReality";
	private static final DecimalFormat FORMAT					= new DecimalFormat("#.##");
	private static final String KM_LABEL				= " kms";
	private static final String END_TEXT				= FORMAT.format(AugmentedReality.MAX_ZOOM) + KM_LABEL;
	private static final int			END_TEXT_COLOR			= Color.WHITE;

	private static WakeLock wakeLock				= null;
	private static CameraSurface camScreen				= null;
	private static SeekBar myZoomBar				= null;
	private static VerticalTextView endLabel				= null;
	private static LinearLayout zoomLayout				= null;
	private static AugmentedView		augmentedView			= null;

	public static final float			MAX_ZOOM				= 5;													// in
																														// KM

	public static boolean				portrait				= false;
	public static boolean				useCollisionDetection	= false;
	public static boolean				showRadar				= false;
	public static boolean				showZoomBar				= true;

	private Context mContext;

	private static AugmentedReality		instance;

	public static AugmentedReality getInstance(Context context) {
		if (instance == null) {
			instance = new AugmentedReality(context);
		}
		return instance;
	}

	public AugmentedReality(Context context) {
		super(context);
		this.mContext = context;
		initAugmentedReality();
	}

	/**
	 * {@inheritDoc}
	 */
	private void initAugmentedReality() {

		camScreen = CameraSurface.getInstance(mContext);
		addView(camScreen);

		augmentedView = new AugmentedView(mContext);
		augmentedView.setOnTouchListener(this);
		FrameLayout.LayoutParams augLayout = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
		addView(augmentedView, augLayout);

		zoomLayout = new LinearLayout(mContext);
		zoomLayout.setVisibility((showZoomBar) ? LinearLayout.VISIBLE : LinearLayout.GONE);
		zoomLayout.setBackgroundResource(R.drawable.transparent);

		LinearLayout textViewLayout = new LinearLayout(mContext);
		textViewLayout.setOrientation(LinearLayout.VERTICAL);
		textViewLayout.setBackgroundResource(R.drawable.black);
		FrameLayout.LayoutParams textViewLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
		textViewLayoutParams.setMargins(20, 0, 20, 0);
		textViewLayoutParams.gravity = Gravity.CENTER;

		VerticalTextView distanceTextView = new VerticalTextView(mContext);
		distanceTextView.setText(mContext.getString(R.string.distance));
		// distanceTextView.setTypeface(BPDPoisUtil.getApplicationFontType(mContext));
		distanceTextView.setTextColor(END_TEXT_COLOR);
		distanceTextView.setGravity(Gravity.CENTER);
		distanceTextView.setTextSize(12);

		endLabel = new VerticalTextView(mContext);
		endLabel.setText(END_TEXT);
		// endLabel.setTypeface(BPDPoisUtil.getApplicationFontType(mContext));
		endLabel.setTextColor(END_TEXT_COLOR);
		endLabel.setGravity(Gravity.CENTER);
		endLabel.setTextSize(18);

		textViewLayout.addView(distanceTextView);
		textViewLayout.addView(endLabel);
		textViewLayout.setPadding(10, 10, 10, 10);

		myZoomBar = new SeekBar(mContext);
		myZoomBar.setIndeterminate(false);
		myZoomBar.setFocusable(true);
		// myZoomBar.setThumb(mContext.getResources().getDrawable(R.drawable.seek_tumb_selector));

		// myZoomBar.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_progress));
		myZoomBar.setMax(500);
		// myZoomBar.setPadding(20, 0, 0, 0);

		// myZoomBar.setThumbOffset(1);
		myZoomBar.setProgress(100);
		myZoomBar.setOnSeekBarChangeListener(myZoomBarOnSeekBarChangeListener);
		LinearLayout.LayoutParams zoomBarParams = new LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.WRAP_CONTENT);
		zoomBarParams.weight = 1;
		zoomBarParams.gravity = Gravity.CENTER;
		zoomBarParams.leftMargin = 20;
		zoomBarParams.rightMargin = 20;
		// zoomBarParams.bottomMargin = 10;
		zoomLayout.addView(myZoomBar, zoomBarParams);
		zoomLayout.addView(textViewLayout, textViewLayoutParams);

		FrameLayout.LayoutParams frameLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM);
		frameLayoutParams.setMargins(20, 10, 20, 10);
		addView(zoomLayout, frameLayoutParams);

		updateDataOnZoom();
		displayZoomText();

		PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");
	}

	public static boolean getPhoneOrientation() {
		return !camScreen.isCameraOrientationPortrait();
	}

	public void releaseAR() {
		camScreen.releaseCamera();
		removeAllViews();
		onStop();
		// removeView(camScreen);
	}

	public boolean isARPresent() {
		if (getChildCount() > 0) return true;
		return false;
	}

	public void resumeAR() {
		camScreen.resumeCamera();
		addView(camScreen);
		addView(augmentedView);
		addView(zoomLayout);
	}

	public void resume() {
		wakeLock.acquire();
	}

	public void pause() {
		wakeLock.release();
	}

	@Override
	public void onSensorChanged(SensorEvent evt) {
		super.onSensorChanged(evt);

		if (evt.sensor.getType() == Sensor.TYPE_ACCELEROMETER || evt.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			augmentedView.postInvalidate();
		}
	}

	private OnSeekBarChangeListener myZoomBarOnSeekBarChangeListener	= new OnSeekBarChangeListener() {
																			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
																				// updateDataOnZoom();
																				displayZoomText();
																				// camScreen.invalidate();
																			}

																			public void onStartTrackingTouch(SeekBar seekBar) {
																				// Ignore
																			}

																			public void onStopTrackingTouch(SeekBar seekBar) {
																				updateDataOnZoom();
																				camScreen.invalidate();
																			}
																		};

	private void displayZoomText() {
		endLabel.setText(String.format("%.2f", calcZoomLevel()) + KM_LABEL);
	}

	private float calcZoomLevel() {
		int myZoomLevel = myZoomBar.getProgress();
		if (myZoomLevel <= 30) myZoomLevel = 30;

		float myout = myZoomLevel / 100f;
		return myout;
	}

	/**
	 * Called when the zoom bar has changed.
	 */
	protected void updateDataOnZoom() {
		float zoomLevel = calcZoomLevel();
		ARData.setRadius(zoomLevel);
		ARData.setZoomLevel(FORMAT.format(zoomLevel));
		ARData.setZoomProgress(myZoomBar.getProgress());
		// endLabel.setText(zoomLevel + " KM");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onTouch(View view, MotionEvent me) {
		// See if the motion event is on a Marker
		if (ARData.getMarkers() != null) {
			for (Marker marker : ARData.getMarkers()) {
				if (marker.handleClick(me.getX(), me.getY())) {
					if (me.getAction() == MotionEvent.ACTION_UP) markerTouched(marker);
					return true;
				}
			}

		}

		return super.onTouchEvent(me);
	};

	protected void markerTouched(Marker marker) {
		if (marker.getViewObject() != null) {
//			Object overlayItemObject = marker.getViewObject();
//			Intent intent = new Intent(mContext, POIViewActivity.class);
//			intent.putExtra(GlobalVar.POINT_OF_INTEREST, (PointOfInterest) overlayItemObject);
//			Location myLocation = ARData.getCurrentLocation();
//			if (myLocation != null) {
//				intent.putExtra(GlobalVar.LATITUDE, myLocation.getLatitude());
//				intent.putExtra(GlobalVar.LONGITUDE, myLocation.getLongitude());
//			}
//			mContext.startActivity(intent);

		}
	}
}
